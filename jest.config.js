module.exports = {
  moduleFileExtensions: ["js", "json", "vue"],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": "vue-jest",
  },
  coverageReporters: [
    "html",
    "text",
    "text-summary",
    "cobertura",
    "lcov",
    "json-summary",
  ],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{js,vue}", "!src/main.js"],
};
