[![pipeline status](https://gitlab.com/karta9821/projekt-magisterski-frontend/badges/main/pipeline.svg)](https://gitlab.com/karta9821/projekt-magisterski-frontend/commits/main)
[![coverage branches](https://karta9821.gitlab.io/projekt-magisterski-frontend/badge-branches.svg)](https://gitlab.com/karta9821/projekt-magisterski-frontend/commits/main)
[![coverage functions](https://karta9821.gitlab.io/projekt-magisterski-frontend/badge-functions.svg)](https://gitlab.com/karta9821/projekt-magisterski-frontend/commits/main)
[![coverage lines](https://karta9821.gitlab.io/projekt-magisterski-frontend/badge-lines.svg)](https://gitlab.com/karta9821/projekt-magisterski-frontend/commits/main)
[![coverage statements](https://karta9821.gitlab.io/projekt-magisterski-frontend/badge-statements.svg)](https://gitlab.com/karta9821/projekt-magisterski-frontend/commits/main)
# Projekt Magisterski - Frontend

## Technologies
- `vue` as our JavaScript Framework
- `vuetify` as Vue UI Library

## Running the Service

In the first step, verify that the docker and docker-compose are working correctly.


Now we can build application:

`docker-compose build`

and run:

`docker-compose up`

The service will now be running on:

`localhost:8080`

