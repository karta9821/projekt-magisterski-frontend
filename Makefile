install: ## Install requrirements packages
	npm install

COMPOSE_FILE_PATH := -f docker-compose.yml
help:           ## Show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

build: ## Build developer docker images
	docker-compose build

install-pre-commit: ## Install pre-commit
	pre-commit install

lint: ## Run linters
	pre-commit run -a

restart:
	@make -s stop
	@make -s up

stop: ## Stop developer docker images
	@docker-compose stop

test: ## Run tests
	npm run test:unit

up: ## Run developer docker images
	docker-compose $(COMPOSE_FILE_PATH) up -d