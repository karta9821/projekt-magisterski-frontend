FROM node:16.13-alpine3.15

WORKDIR /opt/

COPY package*.json ./

RUN npm install
RUN npm install @vue/cli -g

CMD ["npm", "run", "serve"]